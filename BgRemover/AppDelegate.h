//
//  AppDelegate.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 26..
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) IBOutlet UIWindow *window;

@end

