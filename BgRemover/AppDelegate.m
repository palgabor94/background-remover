//
//  AppDelegate.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 26..
//

#import "AppDelegate.h"
#import "BaseNavigationController.h"
#import "DashboardViewController.h"
#import "BaseViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    BaseViewController *vc = [[BaseViewController alloc] init];

    BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:vc];
    vc.navigationController = navigationController;
    [self.window setRootViewController: navigationController];
    [self.window makeKeyAndVisible];
    [vc.navigationController navigateToDashboardScreen];
    return YES;
}

@end
