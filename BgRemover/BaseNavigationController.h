//
//  BaseNavigationController.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 26..
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseNavigationController : UINavigationController <UINavigationControllerDelegate>

- (void)navigateBack;
- (void)navigateToSplashScreen;
- (void)navigateToMainScreen;
- (void)navigateToDashboardScreen;
- (void)navigateToShareScreenWithImage:(UIImage *)image isBackground:(BOOL)isBackground;

@end

NS_ASSUME_NONNULL_END
