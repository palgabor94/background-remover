//
//  BaseNavigationController.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 26..
//

#import "BaseNavigationController.h"
#import "ShareViewController.h"
#import "DashboardViewController.h"

@implementation BaseNavigationController

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];

    if (self == nil)
        return nil;

    return self;
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    return [super popViewControllerAnimated:animated];
}

- (void)navigateBack
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];

    [self popViewControllerAnimated:YES];
}

- (void)navigateToDashboardScreen
{
    DashboardViewController *dashboardScreen = [[DashboardViewController alloc] init];

    //shareScreen.imageView.image = image;

    [super pushViewController:dashboardScreen animated:YES];
}

- (void)navigateToShareScreenWithImage:(UIImage *)image isBackground:(BOOL)isBackground
{
    ShareViewController *shareScreen = [[ShareViewController alloc] init];
    
    shareScreen.selectedImage = image;
    shareScreen.isBackGround = isBackground;

    [super pushViewController:shareScreen animated:YES];
}

@end

