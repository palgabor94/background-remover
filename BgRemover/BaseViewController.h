//
//  BaseViewController.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 10. 11..
//

#import <UIKit/UIKit.h>
#import "BaseNavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

@property (nonatomic, strong) BaseNavigationController *navigationController;
@property (nonatomic, strong) UIWindow *window;

@end

NS_ASSUME_NONNULL_END
