//
//  BaseViewController.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 10. 11..
//

#import "BaseViewController.h"
#import "BaseNavigationController.h"


@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.window = [[[UIApplication sharedApplication] delegate] window];
    self.window.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController = (BaseNavigationController *)(self.window.rootViewController);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
