//
//  CollectionViewCell.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 28..
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@end
