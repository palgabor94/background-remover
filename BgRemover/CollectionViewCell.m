//
//  CollectionViewCell.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 28..
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

#pragma mark - Accessors
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_imageView.layer setMasksToBounds:YES];
    }
    return _imageView;
}

- (UIActivityIndicatorView *)_indicator {
    if (!_indicator) {
        _indicator = [[UIActivityIndicatorView alloc] initWithFrame:self.contentView.bounds];
        _indicator.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _indicator.contentMode = UIViewContentModeScaleAspectFill;
        [_indicator.layer setMasksToBounds:YES];
    }
    return _indicator;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.indicator];
    }
    return self;
}

@end
