//
//  DashboardViewController.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 27..
//

#import <UIKit/UIKit.h>
#import "BaseNavigationController.h"
#import "BaseViewController.h"
#import "CHTCollectionViewWaterfallLayout.h"

@interface DashboardViewController : BaseViewController <UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) BaseNavigationController *baseNavigationController;

@end
