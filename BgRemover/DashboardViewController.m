//
//  DashboardViewController.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 27..
//

#import "DashboardViewController.h"
#import "BaseNavigationController.h"
#import "CollectionViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "EditorViewController.h"

@interface DashboardViewController ()

@property (nonatomic, assign) BOOL isGalleryViewUp;

@property (weak, nonatomic) IBOutlet UIView *galleryView;

@property (nonatomic, strong) NSMutableArray<PHAsset *> *assetList;
@property (nonatomic, strong) PHImageRequestOptions *requestOption;
@property (nonatomic, strong) PHFetchResult<PHAsset *> *collectionResult;
@property (nonatomic, strong) BaseNavigationController *navigationController;
@property (nonatomic, strong) PHImageManager *manager;
@property(nonatomic, strong) UICollectionView *galleryCollectionView;

@end

@implementation DashboardViewController

- (instancetype)init
{
   self = [super initWithNibName:@"DashboardScreen" bundle:nil];
   if (self != nil)
   {
       // Further initialization if needed
   }
   return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.baseNavigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupGallery];
    [self fetchGallery];
}

- (void)setupGallery
{
    _assetList = [NSMutableArray new];
    _manager = [PHImageManager defaultManager];

    CHTCollectionViewWaterfallLayout *collectionView = [[CHTCollectionViewWaterfallLayout alloc] init];

    collectionView.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    collectionView.footerHeight = 10;
    collectionView.minimumColumnSpacing = 10;
    collectionView.minimumInteritemSpacing = 10;
    collectionView.columnCount = 3;
    _galleryCollectionView = [[UICollectionView alloc] initWithFrame:self.galleryView.bounds collectionViewLayout:collectionView];
    _galleryCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _galleryCollectionView.dataSource = self;
    _galleryCollectionView.delegate = self;
    _galleryCollectionView.backgroundColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    [_galleryCollectionView registerClass:[CollectionViewCell class]
               forCellWithReuseIdentifier:@"cell"];

    [self.galleryView addSubview:_galleryCollectionView];
}

- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];


    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
    options.synchronous = NO;
    options.networkAccessAllowed = NO;
    [_manager requestImageForAsset:_assetList[indexPath.row]
                        targetSize:CGSizeMake(250, 250)
                       contentMode:PHImageContentModeDefault
                           options:options
                     resultHandler:^void(UIImage *image, NSDictionary *info) {

        if (image)
        {
            cell.imageView.image = image;

        }
    }];
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _assetList.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{

        return CGSizeMake(100, 100);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
    options.synchronous = NO;
    options.networkAccessAllowed = YES;
    options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {

    };

    [_manager requestImageForAsset:_assetList[indexPath.row]
                        targetSize:PHImageManagerMaximumSize
                       contentMode:PHImageContentModeDefault
                           options:options
                     resultHandler:^void(UIImage *image, NSDictionary *info) {
        if (image)
        {
//            EditorViewController *mainVC = [[EditorViewController alloc] init];
//            mainVC.selectedImage = image;
//            [self.baseNavigationController pushViewController:mainVC animated:YES];
            
            EditorViewController *secondViewCtrl = [[EditorViewController alloc] initWithNibName:@"EditorScreen" bundle:nil];
            secondViewCtrl.selectedImage = image;
               [self.navigationController pushViewController:secondViewCtrl animated:YES];
        }
    }];
}

- (void)fetchGallery
{
    __weak typeof(self) welf = self;

    welf.assetList = [NSMutableArray new];

    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status != PHAuthorizationStatusAuthorized)
    {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status != PHAuthorizationStatusAuthorized)
            {
                //User don't give us permission. Showing alert with redirection to settings
                //Getting description string from info.plist file
                NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:cancelAction];

                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }];
                [alertController addAction:settingsAction];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];

                });

            }
            else if (status == PHAuthorizationStatusAuthorized)
            {
                [self getPicturesFromGallery];

            }
        }];

    } else {
        [self getPicturesFromGallery];
    }
}

- (void)getPicturesFromGallery
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
            PHAssetCollection *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                                 subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                                 options:fetchOptions].firstObject;

            fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];

            self.collectionResult = [PHAsset fetchAssetsInAssetCollection:result options:fetchOptions];

            [self.collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop)
             {
                if (asset.mediaType == PHAssetMediaTypeImage && asset.sourceType == PHAssetSourceTypeUserLibrary)
                {
                    [self.assetList addObject:asset];
                }
            }];

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.galleryCollectionView reloadData];
            });
            dispatch_async(dispatch_get_main_queue(), ^{
            });
        });
    }
}

- (void)showGalleryScreen:(BOOL)show
{
    if(show)
    {
        
        if(!_isGalleryViewUp)
        {
            _galleryView.hidden = NO;
            self.galleryView.frame = CGRectOffset(self.galleryView.frame, 0, 300);
            [UIView animateWithDuration:0.2
                             animations:^{
                self.galleryView.frame = CGRectOffset(self.galleryView.frame, 0, -300);
            }
                             completion:^(BOOL finished){
                self.isGalleryViewUp = YES;
            }];
        }
    } else {
        if(_isGalleryViewUp)
        {
            [UIView animateWithDuration:0.2
                             animations:^{
                self.galleryView.frame = CGRectOffset(self.galleryView.frame, 0, 300);
            }
                             completion:^(BOOL finished){
                self.galleryView.frame = CGRectOffset(self.galleryView.frame, 0, -300);
                self.isGalleryViewUp = NO;
                self.galleryView.hidden = YES;
            }];
        }
    }
}

- (IBAction)choosePhotoTapped:(UITapGestureRecognizer *)sender
{
    [self showGalleryScreen:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
