//
//  EditorViewController.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

#import <UIKit/UIKit.h>
#import "BaseNavigationController.h"
#import "BaseViewController.h"

@interface EditorViewController : BaseViewController <UIImagePickerControllerDelegate>

@property (nonatomic, strong) BaseNavigationController *baseNavigationController;

@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *foregroundImageView;

@property (nonatomic, weak) IBOutlet UIImageView *maskView;
@property (weak, nonatomic) IBOutlet UIView *croppedView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

//@property (nonatomic, weak) IBOutlet UIImageView *backgroundView;
@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

@property (nonatomic, weak) IBOutlet UIButton *fodButton;
@property (nonatomic, weak) IBOutlet UIButton *foregroundButton;
@property (weak, nonatomic) IBOutlet UIButton *backgroundButton;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIView *scrollView;

@property (weak, nonatomic) IBOutlet UIStackView *maskStackView;

@property (nonatomic, strong) NSMutableArray *pathArray;

@property (nonatomic, assign) CGPoint lastPoint;


@end

