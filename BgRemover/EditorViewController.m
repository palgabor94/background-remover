//
//  EditorViewController.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

#import "EditorViewController.h"
#import "ForegroundObjectDetector.h"
#import "ShareViewController.h"
#import "BaseNavigationController.h"
#import <Photos/Photos.h>
#import "UIImage+Filtering.h"


@interface EditorViewController ()

@property (nonatomic, strong) BaseNavigationController *navigationController;

@property (nonatomic, strong) ForegroundObjectDetector *detector;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;

@property (nonatomic, strong) UIImage *maskImage;
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *originalForegroundImage;
@property (nonatomic, strong) UIImage *filteredBackgroundImage;

@property (nonatomic, strong) NSMutableArray *backgroundImages;

@property (nonatomic, assign) BOOL isBackGround;
@property (nonatomic, assign) BOOL isBrushMode;
@property (nonatomic, assign) BOOL isMaskZoom;
@property (nonatomic, assign) BOOL isPortre;
@property (nonatomic, assign) BOOL isEraseMode;
@property (nonatomic, strong) UIBezierPath *path;

@property (nonatomic, assign) BOOL isContrastChanged;
@property (nonatomic, assign) BOOL isBlurChanged;
@property (nonatomic, assign) BOOL isBrightnessChanged;
@property (nonatomic, assign) float blurValue;
@property (nonatomic, assign) float contrastValue;
@property (nonatomic, assign) float brightnessValue;

@property (nonatomic, assign) int currentPage;
@property (nonatomic, assign) float colorValue;

@property (nonatomic, strong) UIScrollView *someScrollView;

@property (weak, nonatomic) IBOutlet UIStackView *adjustmentStackView;

@property (weak, nonatomic) IBOutlet UISwitch *backgroundSwitch;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *croppedViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *croppedViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *foregroundImageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *foregroundImageHeight;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@end

@implementation EditorViewController

- (instancetype)init
{
   self = [super initWithNibName:@"EditorScreen" bundle:nil];
   if (self != nil)
   {
       // Further initialization if needed
   }
   return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setup];
    [self setupScrollView];
    [self changeForegroundColor];
   
}

- (void)setup
{
    _imagePickerController = [[UIImagePickerController alloc] init];

    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePickerController.delegate = self;
    [_imageView setImage:_selectedImage];
    
    self.colorValue = 0.3;
    self.currentPage = 0;

    [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    //self.backgroundButton.hidden = YES;
    //self.foregroundButton.hidden = YES;
    self.fodButton.hidden = NO;
    self.scrollView.hidden = YES;
    self.imageView.userInteractionEnabled = NO;
    self.foregroundImageView.userInteractionEnabled = NO;
    self.foregroundImageView.hidden = YES;
    //self.foregroundImageView.frame = self.imageView.frame;
    self.isBackGround = YES;

    
    self.pathArray=[[NSMutableArray alloc]init];
    
    if(self.imageView.image.size.height > self.imageView.image.size.width)
    {
        self.isPortre = YES;
    } else {
        //self.croppedViewWidth.constant = self.scrollView.frame.size.width;
    }
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];

    if ([touch.view isEqual: self.view] || touch.view == nil) {
        return;
    }
    
    if(_isBrushMode)
    {
        self.path = [[UIBezierPath alloc] init];
        self.path.lineWidth = 1;
        
        UITouch *touch = [touches anyObject];
        CGPoint p = [touch locationInView:self.maskView];
        p.x = p.x * (self.maskView.image.size.width/self.maskView.frame.size.width);
        p.y = p.y * (self.maskView.image.size.height/self.maskView.frame.size.height);
        //CGPoint tapPointInView = [_zoomScrollView convertPoint:p toView:self.maskView];
        //CGPoint currentPoint2 = [touch locationInView:self.maskView];
        [self.path moveToPoint:p];
        
        [_pathArray addObject:self.path];
        
        _lastPoint = [touch locationInView:self.maskView];
    }
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    
    if(_isBrushMode)
    {
                UIGraphicsBeginImageContext(self.maskView.frame.size);
                [self.maskImage drawInRect:CGRectMake(0, 0, self.maskView.frame.size.width, self.maskView.frame.size.height)];
        
                CGPoint p = [touch locationInView:self.maskView];
                [_path addLineToPoint:p];
        
                [[UIColor redColor] setStroke];
                [[UIColor redColor] setFill];
        
                //[_path fillWithBlendMode:kCGBlendModeNormal alpha:1.0];
        
                [_path fillWithBlendMode:self.isEraseMode ? kCGBlendModeClear :  kCGBlendModeNormal alpha:1.0];
        
        
                self.maskView.image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_isBrushMode)
    {
        [self.path removeAllPoints];
        
        self.maskImage = self.maskView.image;
    }
}

- (void)applyMask
{
    [self makeForeground:self.imageView.image];
    //self.backgroundButton.hidden = NO;
    //self.foregroundButton.hidden = NO;
    self.fodButton.hidden = YES;
    self.scrollView.hidden = NO;
    self.applyButton.hidden = YES;
    self.maskStackView.hidden = YES;
    self.nextButton.hidden = NO;
    self.bottomView.hidden = NO;
    self.foregroundImageView.hidden = NO;
    self.foregroundImageView.userInteractionEnabled = NO;
    self.croppedView.userInteractionEnabled = NO;
    self.scrollView.userInteractionEnabled = YES;
}

- (IBAction)backgroundSwitchTapped:(UISwitch *)sender
{
    if(sender.isOn)
    {
        //is background
        self.isBackGround = YES;
        self.scrollView.hidden = NO;
        self.adjustmentStackView.hidden = NO;
        self.foregroundImageView.backgroundColor = [UIColor clearColor];
    } else {
        //no background
        self.isBackGround = NO;
        self.scrollView.hidden = YES;
        self.adjustmentStackView.hidden = YES;
        self.foregroundImageView.backgroundColor = [UIColor whiteColor];
        
    }
}


- (IBAction)applyTapped:(UIButton *)sender
{
    [self applyMask];
}

- (IBAction)nextTapped:(UIButton *)sender
{
    [self create];
}

- (IBAction)backTapped:(UIButton *)sender
{
    [self.navigationController navigateBack];
}


- (IBAction)fod:(id)sender {
    self.loadingView.hidden = NO;
    [self.loadingIndicator startAnimating];
    _detector = [ForegroundObjectDetector new];
    [_detector detectForeground:self.imageView.image withCompletion:^(UIImage *mask) {
        self.loadingView.hidden = YES;
        [self.loadingIndicator stopAnimating];
        self.maskView.image = mask;
        self.applyButton.hidden = NO;
        self.isBrushMode = YES;
        self.fodButton.hidden = YES;
        self.maskStackView.hidden = NO;
        self.maskView.userInteractionEnabled  = YES;

        if(_isPortre)
        {
            UIGraphicsBeginImageContext(self.maskView.frame.size);
            [self.maskView.image drawInRect:CGRectMake((self.maskView.frame.size.width-[self frameForImage:self.maskView.image inImageViewAspectFit:self.maskView])/2, 0, self.maskView.frame.size.width-(self.maskView.frame.size.width-[self frameForImage:self.maskView.image inImageViewAspectFit:self.maskView]), self.maskView.frame.size.height)];
        } else {
            UIGraphicsBeginImageContext(self.maskView.frame.size);
            [self.maskView.image drawInRect:CGRectMake(0, (self.maskView.frame.size.height-[self frameForImage:self.maskView.image inImageViewAspectFit:self.maskView])/2, self.maskView.frame.size.width, self.maskView.frame.size.height-(self.maskView.frame.size.height-[self frameForImage:self.maskView.image inImageViewAspectFit:self.maskView]))];
        }
        
        self.maskView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.maskImage = self.maskView.image;
        
        
    }];
}

//- (IBAction)openImagePicker:(id)sender
//{
//
//    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
//    if (status == PHAuthorizationStatusAuthorized)
//    {
//        [self presentViewController:_imagePickerController animated:YES completion:nil];
//
//    }
//
//    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//        if (status != PHAuthorizationStatusAuthorized)
//        {
//            //User don't give us permission. Showing alert with redirection to settings
//            //Getting description string from info.plist file
//            NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
//            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//            [alertController addAction:cancelAction];
//
//            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//            }];
//            [alertController addAction:settingsAction];
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
//
//            });
//
//        }
//        else if (status == PHAuthorizationStatusAuthorized)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                [self presentViewController:_imagePickerController animated:YES completion:nil];
//            });
//        }
//    }];
//
//}

- (IBAction)burshTapped:(UIButton *)sender
{
    self.isEraseMode = NO;
}

- (IBAction)eraseTapped:(UIButton *)sender
{
    self.isEraseMode = YES;
}


- (IBAction)foregroundTapped:(UIButton *)sender
{
    self.scrollView.userInteractionEnabled = NO;
    self.foregroundImageView.userInteractionEnabled = YES;
    self.croppedView.userInteractionEnabled = YES;
}

- (IBAction)backgroundTapped:(UIButton *)sender
{
    self.croppedView.userInteractionEnabled = NO;
    self.scrollView.userInteractionEnabled = YES;
    self.foregroundImageView.userInteractionEnabled = NO;
}

- (IBAction)colorSliderChanged:(UISlider *)sender
{
    self.colorValue = sender.value;
    [self changeForegroundColor];
}

- (IBAction)blurSliderChanged:(UISlider *)sender
{
        self.isBlurChanged = YES;
        self.blurValue = sender.value;
        [self applyFilterOnImage];
    
}

- (IBAction)brightnessSliderChanged:(UISlider *)sender
{
    self.isBrightnessChanged = YES;
    self.brightnessValue = sender.value;
    [self applyFilterOnImage];
}

- (IBAction)contrastSliderChanged:(UISlider *)sender
{
    if(sender.value  == 0)
    {
        self.isContrastChanged = NO;
        self.contrastValue = sender.value;
        [self applyFilterOnImage];
    } else {
        self.isContrastChanged = YES;
        self.contrastValue = sender.value;
        [self applyFilterOnImage];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imageOrig = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.imageView.image = imageOrig;

    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)makeForeground:(UIImage *)image
{

    UIImage *layeredImage;
    if(self.isPortre)
    {
        UIGraphicsBeginImageContext(image.size);
        [image drawInRect:CGRectMake(0, 0.0, image.size.width, image.size.height)];
        [self.maskView.image drawInRect:CGRectMake(-((image.size.height-image.size.width)/2), 0.0, image.size.height, image.size.height) blendMode:kCGBlendModeDestinationIn alpha:1];
        layeredImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    } else {
        UIGraphicsBeginImageContext(image.size);
        [image drawInRect:CGRectMake(0, 0.0, image.size.width, image.size.height)];
        [self.maskView.image drawInRect:CGRectMake(0, -((image.size.width-image.size.height)/2), image.size.width, image.size.width) blendMode:kCGBlendModeDestinationIn alpha:1];
        layeredImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }

    self.originalForegroundImage = layeredImage;
    self.foregroundImageView.image = layeredImage;
    self.imageView.hidden = YES;
    self.maskView.hidden = YES;
    
}

- (void)create
{
    int imageWidth = 0;
    int imageHeight;
    
    if(self.isPortre)
    {
        imageHeight = 720;
        imageWidth = imageHeight*(self.imageView.image.size.width/self.imageView.image.size.height);
    } else {
        imageWidth = 720;
        imageHeight = imageWidth*(self.imageView.image.size.height/self.imageView.image.size.width);
    }
    
    CGSize imageSize = CGSizeMake(imageWidth, imageHeight);
    UIImage *createdImage;
    
    if(self.isBackGround)
    {
        UIGraphicsBeginImageContext(imageSize);
        [self.filteredBackgroundImage drawInRect:CGRectMake(0, 0.0, imageSize.width, imageSize.height)];
        [self.foregroundImageView.image drawInRect:CGRectMake(0.0, 0.0, imageSize.width, imageSize.height) blendMode:kCGBlendModeNormal alpha:1];
        createdImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    } else {
        UIGraphicsBeginImageContext(imageSize);
        [self.foregroundImageView.image drawInRect:CGRectMake(0.0, 0.0, imageSize.width, imageSize.height) blendMode:kCGBlendModeNormal alpha:1];
        createdImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    [self.navigationController navigateToShareScreenWithImage:createdImage isBackground:self.isBackGround];
}

- (void)setupScrollView
{
    self.someScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,self.scrollView.frame.size.width,self.scrollView.frame.size.height)];
    self.someScrollView.pagingEnabled = YES;
    [self.someScrollView setAlwaysBounceVertical:NO];
    
    NSInteger numberOfPages = 5;
    
    for (int i = 0; i < numberOfPages; i++)
    {
        CGFloat xOrigin = i * self.scrollView.frame.size.width;
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:
                              CGRectMake(xOrigin, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];

        self.backgroundImages = [NSMutableArray array];
        
        if(self.backgroundImages.count == 0)
        {
            if(self.isPortre)
            {
                [[[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:nil] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
                    NSString *path = [obj lastPathComponent];
                    if ([path hasPrefix:@"portreBG"]) {
                        [self.backgroundImages addObject:path];
                    }
                }];
            } else {
                [[[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:nil] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
                    NSString *path = [obj lastPathComponent];
                    if ([path hasPrefix:@"landscapeBG"]) {
                        [self.backgroundImages addObject:path];
                    }
                }];
            }
        }
        
        image.image = [UIImage imageNamed:self.backgroundImages[i]];
        image.backgroundColor = [UIColor grayColor];
        
        if(self.isPortre)
        {
            CGRect currentFrame = image.bounds;
            currentFrame.size.width = self.imageView.frame.size.width-(self.imageView.frame.size.width-[self frameForImage:self.imageView.image inImageViewAspectFit:self.imageView]);
            
            image.bounds = currentFrame;
        } else {
            
            CGRect currentFrame = image.bounds;
            currentFrame.size.height = self.imageView.frame.size.height-(self.imageView.frame.size.height-[self frameForImage:self.imageView.image inImageViewAspectFit:self.imageView]);
            
            image.bounds = currentFrame;
            
        }
        self.foregroundImageHeight.constant = self.mainView.bounds.size.height;
        self.foregroundImageWidth.constant = self.mainView.bounds.size.width;
        
        image.clipsToBounds = YES;
        image.contentMode = UIViewContentModeScaleAspectFill;
        [self.someScrollView addSubview:image];
        
        if(self.isPortre)
        {
            self.croppedViewWidth.constant = self.foregroundImageWidth.constant;
            self.foregroundImageHeight.constant = self.mainView.frame.size.height;
            self.foregroundImageWidth.constant = self.mainView.frame.size.width;
        } else {
            self.croppedViewHeight.constant = self.foregroundImageWidth.constant;
            self.foregroundImageHeight.constant = self.mainView.bounds.size.height;
            self.foregroundImageWidth.constant = self.mainView.bounds.size.width;
        }
    }
    //set the scroll view content size
    self.someScrollView.backgroundColor = [UIColor clearColor];
    self.someScrollView.contentSize = CGSizeMake(self.scrollView.frame.size
                                            .width *
                                            numberOfPages,
                                            self.scrollView.frame.size.height);
    //add the scrollview to this view
    [self.scrollView addSubview:self.someScrollView];
    self.someScrollView.delegate = self;
    //[self.scrollView bringSubviewToFront:self.foregroundImageView];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self changeForegroundColor];
}

- (void)changeForegroundColor
{
    self.currentPage = self.someScrollView.contentOffset.x / self.someScrollView.frame.size.width;
    int i = 0;
    for(UIImageView *image in self.someScrollView.subviews)
    {
        if(i == self.currentPage)
        {
            self.backgroundImage = [UIImage imageNamed:self.backgroundImages[i]];
            self.filteredBackgroundImage = [UIImage imageNamed:self.backgroundImages[i]];
            NSString *imgName = self.backgroundImages[i];
            NSString * newString;
            if(self.isPortre)
            {
                newString = [imgName substringWithRange:NSMakeRange(imgName.length-10, 6)];
                self.foregroundImageHeight.constant = self.mainView.bounds.size.height;
                self.foregroundImageWidth.constant = self.mainView.bounds.size.width;
            } else {
                newString = [imgName substringWithRange:NSMakeRange(imgName.length-10, 6)];
            }
            UIColor *color = [self colorWithHexString:newString];
            self.foregroundImageView.image = [self overlayImage:self.originalForegroundImage withColor:color withColorAlpha:(float)self.colorValue withBlendMode:kCGBlendModeMultiply];
            self.foregroundImageHeight.constant = self.mainView.bounds.size.height;
            self.foregroundImageWidth.constant = self.mainView.bounds.size.width;
        }
        i++;
    }
}

- (float)frameForImage:(UIImage*)image inImageViewAspectFit:(UIImageView*)imageView
{
    float imageRatio = image.size.width / image.size.height;
    float viewRatio = imageView.frame.size.width / imageView.frame.size.height;
    if(imageRatio < viewRatio)
    {
        float scale = imageView.frame.size.height / image.size.height;
        float width = scale * image.size.width;
        float topLeftX = (imageView.frame.size.width - width) * 0.5;
        return width;
    }
    else
    {
        float scale = imageView.frame.size.width / image.size.width;
        float height = scale * image.size.height;
        float topLeftY = (imageView.frame.size.height - height) * 0.5;

        return height;
    }
}

- (UIImage *)overlayImage:(UIImage *)image withColor:(UIColor *)color withColorAlpha:(float)alpha withBlendMode:(CGBlendMode)blendMode
{
    UIGraphicsBeginImageContext(image.size);

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    // draw black background to preserve color of transparent pixels
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [[UIColor whiteColor] setFill];
    CGContextFillRect(context, rect);
    // draw original image
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, image.CGImage);
    // tint image (loosing alpha) - the luminosity of the original image is preserved
    CGContextSetBlendMode(context, blendMode);
    CGContextSetAlpha(context, alpha);
    [color setFill];
    CGContextFillRect(context, rect);
    // mask by alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextSetAlpha(context, 1);
    CGContextDrawImage(context, rect, image.CGImage);
    //[firstImage drawInRect:rect];

    // image drawing code here
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return coloredImage;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (UIColor *)colorWithHexString:(NSString *)stringToConvert
{
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $

    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;

    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}

- (UIImage *)blurredImageWithImage:(UIImage *)sourceImage withValue:(float)value{

    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];

    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:value] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];

    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];

    UIImage *blurredImage = [UIImage imageWithCGImage:cgImage];

    if (cgImage) {
        CGImageRelease(cgImage);
    }

    return blurredImage;
}

- (UIImage *)contrastImageWithImage:(UIImage *)sourceImage withValue:(float)value{
    
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIColorControls"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:value] forKey:@"inputContrast"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    
    if (cgImage) {
        CGImageRelease(cgImage);
    }
    
    return retVal;
}

- (UIImage *)brightnessImageWithImage:(UIImage *)sourceImage withValue:(float)value{
    
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIColorControls"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:value] forKey:@"inputBrightness"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    
    if (cgImage) {
        CGImageRelease(cgImage);
    }
    
    return retVal;
}

- (void)applyFilterOnImage
{
    __weak typeof(self) welf = self;
    
    UIImage *sourceImage = welf.backgroundImage;
    
    if (welf.isBlurChanged)
    {
        //[blurFilter setValue:@(self.blurValue) forKey:@"inputRadius"];
        sourceImage = [self blurredImageWithImage:sourceImage withValue:self.blurValue*100/2];
    }
    
    if (welf.isContrastChanged)
    {
        sourceImage = [self contrastImageWithImage:sourceImage withValue:self.contrastValue];
    }
    
    if (welf.isBrightnessChanged)
    {
        sourceImage = [self brightnessImageWithImage:sourceImage withValue:self.brightnessValue];
    }
    
    self.currentPage = self.someScrollView.contentOffset.x / self.someScrollView.frame.size.width;
    int i = 0;
    for(UIImageView *image in self.someScrollView.subviews)
    {
        if(i == self.currentPage)
        {
            image.image = sourceImage;
        }
        i++;
    }
    _filteredBackgroundImage = sourceImage;
}

@end
