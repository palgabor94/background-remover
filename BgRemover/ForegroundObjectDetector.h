//
//  ForegroundObjectDetector.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface ForegroundObjectDetector : NSObject

- (void)detectForeground:(UIImage *)image withCompletion:(void (^)(UIImage *mask))completionBlock;

@end

NS_ASSUME_NONNULL_END
