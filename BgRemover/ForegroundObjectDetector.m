//
//  ForegroundObjectDetector.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

#import "ForegroundObjectDetector.h"
#import "TorchModule.h"
#import "BgRemover-Swift.h"

@interface ForegroundObjectDetector ()

@property (nonatomic, strong) TorchModule *module;
@end

@implementation ForegroundObjectDetector

- (void)setup
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"fod" ofType:@"p"];
    self.module =  [[TorchModule alloc] initWithFileAtPath:path];
}

- (void)detectForeground:(UIImage *)image withCompletion:(void (^)(UIImage *mask))completionBlock
{
//    if (!self.module)
//    {
//        [self setup];
//    }

    UIImage *resizedimage = [image resizedTo:CGSizeMake(320, 320) scale:1];
    NSArray *normImage = [resizedimage normalized];
    ForegroundPredictor *fod = [[ForegroundPredictor alloc] init];

    UIImage *newMask = [fod predictImageWithImage:image];

    completionBlock(newMask);
}


@end
