//
//  ForegroundPredictor.swift
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

import Foundation
import UIKit

@objc public class ForegroundPredictor : NSObject {
    private lazy var module: TorchModule = {
        if let filePath = Bundle.main.path(forResource: "fod", ofType: "p"),
            let module = TorchModule(fileAtPath: filePath) {
            return module
        } else {
            fatalError("Can't find the model file!")
        }
    }()

    @objc func predictImage(image: UIImage) -> UIImage {
        let originalWidth = image.size.width
        let originalHeight = image.size.height

        let resizedImage = image.resized(to: CGSize(width: 320, height: 320))
        guard var pixelBuffer = resizedImage.normalized() else {
            return UIImage.init()
        }

        guard let outputs = module.predict(image: UnsafeMutableRawPointer(&pixelBuffer)) else {
            return UIImage.init()
        }

        var finalImage = tensorToBitmap(tensor: outputs)
        return finalImage.resized(to: CGSize(width: originalWidth, height: originalHeight))
    }



    func tensorToBitmap(tensor: [NSNumber]) -> UIImage  {

        var floatArray: [Float] = tensor.map { (number) -> Float in
            return number.floatValue
        }
        print(tensor)

        var intArray: [Int] = [Int]()
        var colorArray: [PixelData] = [PixelData]()

        let pixelCount: Int = 320*320
        let max = floatArray.max()
        let min = floatArray.min()

        for (index, element) in floatArray.enumerated() {
            floatArray[index] = (floatArray[index]-min!)/(max!-min!)
        }


        for (index, element) in floatArray.enumerated() {
            let c = element
            let p: Int = Int(c * 255.0)

            var test = (p << 24) | (p & 0xff) << 16 | (p & 0xff) << 8
            test = test | (p & 0xff)

            let blue = test & 0xFF
            let green =  (test >> 8) & 0xFF
            let red =  (test >> 16) & 0xFF
            let alpha =  (test >> 24) & 0xFF

            let pixel = UIColor(red: CGFloat(red)/255, green: CGFloat(green)/255,
                                blue: CGFloat(blue)/255, alpha: CGFloat(alpha)/255)

            let pixelData = PixelData(a: UInt8(alpha), r: UInt8(red), g: UInt8(0), b: UInt8(0))
            colorArray.append(pixelData)
        }

        var image: UIImage = self.imageFromARGB32Bitmap(pixels: colorArray, width: 320, height: 320)!
        return image
    }

    public struct PixelData {
        var a: UInt8
        var r: UInt8
        var g: UInt8
        var b: UInt8
    }

    func imageFromARGB32Bitmap(pixels: [PixelData], width: Int, height: Int) -> UIImage? {
        guard width > 0 && height > 0 else { return nil }
        guard pixels.count == width * height else { return nil }

        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
        let bitsPerComponent = 8
        let bitsPerPixel = 32

        var data = pixels // Copy to mutable []
        guard let providerRef = CGDataProvider(data: NSData(bytes: &data,
                                length: data.count * MemoryLayout<PixelData>.size)
            )
            else { return nil }

        guard let cgim = CGImage(
            width: width,
            height: height,
            bitsPerComponent: bitsPerComponent,
            bitsPerPixel: bitsPerPixel,
            bytesPerRow: width * MemoryLayout<PixelData>.size ,
            space: rgbColorSpace,
            bitmapInfo: bitmapInfo,
            provider: providerRef,
            decode: nil,
            shouldInterpolate: true,
            intent: .defaultIntent
            )
            else { return nil }

        return UIImage(cgImage: cgim)
    }
}

