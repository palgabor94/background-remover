//
//  ShareViewController.h
//  BgRemover
//
//  Created by Gábor Pál on 2021. 10. 11..
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "BaseNavigationController.h"

@interface ShareViewController : BaseViewController

@property (nonatomic, strong) BaseNavigationController *baseNavigationController;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, assign) BOOL isBackGround;

@end
