//
//  ShareViewController.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 10. 11..
//

#import "ShareViewController.h"
#import "BaseNavigationController.h"

@interface ShareViewController ()

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (nonatomic, strong) BaseNavigationController *navigationController;

@property (nonatomic, assign) BOOL isSaved;

@end

@implementation ShareViewController

- (instancetype)init
{
   self = [super initWithNibName:@"ShareScreen" bundle:nil];
   if (self != nil)
   {
       // Further initialization if needed
   }
   return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    // Do any additional setup after loading the view.
}

- (void)setup
{
    self.imageView.image = _selectedImage;
    if(!self.isBackGround)
    {
        self.imageView.backgroundColor = [UIColor whiteColor];
    } else {
        self.imageView.backgroundColor = [UIColor clearColor];
    }
}

- (IBAction)backTapped:(UIButton *)sender
{
    [self.navigationController navigateToDashboardScreen];
}

- (IBAction)saveTapped:(UIButton *)sender
{
    if(!_isSaved)
    {
        self.saveButton.alpha = 0.3;
        UIImageWriteToSavedPhotosAlbum(self.selectedImage, nil, nil, nil);
        self.isSaved = YES;
    }
}

- (IBAction)shareTapped:(UIButton *)sender
{
        NSArray *activityItems = @[self.selectedImage];
        UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityViewControntroller.excludedActivityTypes = @[];
        [self presentViewController:activityViewControntroller animated:true completion:nil];
}

@end
