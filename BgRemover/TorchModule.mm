//
//  TorchModule.m
//  BgRemover
//
//  Created by Gábor Pál on 2021. 09. 30..
//

#import "TorchModule.h"
#import <LibTorch/LibTorch.h>

@implementation TorchModule {
@protected
    torch::jit::script::Module _impl;
}

- (nullable instancetype)initWithFileAtPath:(NSString*)filePath {
  self = [super init];
  if (self) {
    try {
      _impl = torch::jit::load(filePath.UTF8String);
      _impl.eval();
    } catch (const std::exception& exception) {
      NSLog(@"%s", exception.what());
      return nil;
    }
  }
  return self;
}

- (NSArray<NSNumber*>*)predictImage:(void*)imageBuffer {
  try {
    at::Tensor tensor = torch::from_blob(imageBuffer, {1, 3, 320, 320}, at::kFloat);
    torch::autograd::AutoGradMode guard(false);
    at::AutoNonVariableTypeMode non_var_type_mode(true);
    auto outputTuple = _impl.forward({tensor}).toTuple();
    auto outputTensor = outputTuple->elements()[0].toTensor();
    float* floatBuffer = outputTensor.data_ptr<float>();

    NSMutableArray* results = [[NSMutableArray alloc] init];
      
    for (int i = 0; i < 102400; i++) {
        [results addObject:@(floatBuffer[i])];
    }

    return [results copy];
  } catch (const std::exception& exception) {
    NSLog(@"%s", exception.what());
  }
  return nil;
}


@end

